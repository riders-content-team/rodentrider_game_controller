#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState
from std_srvs.srv import Trigger, TriggerResponse
import time
import math
import os
import requests
import subprocess
import atexit
import random
from rodentrider_arena.srv import RemoveModel
from rodentrider_arena.srv import SpawnModel

class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

class GameController:

    def __init__(self, robot_name):

        self.map_width = 20  # map is 20x20

        self.robot_name = robot_name
        self.robot_status = "Paused"

        self.time_passed = 0.0
        self.start_time = 0.0

        self.metrics = {
            'Status' : self.robot_status,
            'Time' : self.time_passed,
        }

        self.git_dir = '/workspace/src/.git'
        commit_id = ''
        riders_project_id = os.environ.get('RIDERS_PROJECT_ID', '')

        # RIDERS_COMMIT_ID
        git_dir_exists = os.path.isdir(self.git_dir)
        if git_dir_exists:

            popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE)
            remote_url, error_remote_url = popen.communicate()

            popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            branch_name, error_branch_name = popen.communicate()

            popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            commit_id, error_commit_id = popen.communicate()

            if error_remote_url is None and remote_url is not None:
                remote_url = remote_url.rstrip('\n').rsplit('@')
                user_name = remote_url[0].rsplit(':')[1][2:]
                remote_name = remote_url[1]

        self.result_metrics = {
            'Status' : self.robot_status,
            'Time' : self.time_passed,
            'commit_id': commit_id.rstrip('\n'),
        }

        rospy.init_node("game_controller")

        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        self.init_services()

        # hack remove older models - cleans up from a reset
        self.remove_wall()
        
        # we must guarantee that above remove_model is received and executes before subsequent
        # calls to spawn the new walls. Otherwise reset can be out of order and subsequent spawn
        # calls end up getting deleted by above calls.
        # What is the clean way to guaranetee the ordering?
        # This time delay mostly likely is not completely safe - this needs to be understood and investigated
        time.sleep(0.5)

        # spawn the special wall
        self.add_wall()

        # create map visit counter
        self.map_visits = [0] * (self.map_width * self.map_width)

        last_index = -1 # track the last point we are in so we can know when we moved
        while ~rospy.is_shutdown():
            resp = self.get_robot_state("rodentrider", "")
            p = resp.pose.position

            # the origin is 0,0 and the lower left is (-self.map_width/2, -self.map_width/2)

            # determine which square we are in
            a = self.map_width/2
            x = int(p.x + a)
            y = int(p.y + a)

            assert x >= 0 and x < self.map_width
            assert y >= 0 and y < self.map_width

            index = x + y * self.map_width

            if index != last_index:
                self.map_visits[index] += 1

                # print 'Visited square (', x, ',', y, ')', self.map_visits[index], 'times'

                # how to detect if they made a 'random' search for checkpoint 1?
                # we just say it's done once they visit the same place twice
                if self.map_visits[index] == 2:
                    self.achieve(113)
                
                # did they finish the maze?
                if x == 0 or x == self.map_width - 1 or y == 0 or y == self.map_width - 1:
                    self.robot_status = "Finished!"

                    # print 'Map Visits', self.map_visits

                    # determine if we did the left wall following correctly
                    self.follow_left_wall_result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 2, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    bAllMatch = True
                    for q in range(self.map_width * self.map_width):
                        if self.follow_left_wall_result[q] != self.map_visits[q]:
                            bAllMatch = False
                            break
                    
                    if bAllMatch:
                        self.achieve(114)
                    
                    # determine if we did the Pledge algorithm correctly
                    bAllMatch = True
                    self.pledge_result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 2, 2, 2, 3, 1, 0, 1, 2, 0, 0, 2, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 2, 3, 2, 2, 2, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 2, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 2, 0, 0, 3, 2, 2, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1, 0, 1, 0, 0, 1, 0, 2, 2, 2, 1, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1, 0, 1, 1, 1, 2, 0, 2, 0, 1, 1, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1, 0, 1, 1, 0, 2, 1, 2, 0, 1, 2, 2, 1, 0, 0, 2, 0, 0, 2, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 2, 2, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    for q in range(self.map_width * self.map_width):
                        if self.pledge_result[q] != self.map_visits[q]:
                            bAllMatch = False
                            break
                    
                    if bAllMatch:
                        self.achieve(115)
            
            last_index = index

            # check if we are in exit point
            if True: # TODO
                if self.robot_status == "Running":
                    self.time_passed = rospy.get_time() - self.start_time
            else:
                self.robot_status = "Finished Course!"

            self.publish_metrics()

            rate.sleep()
            if self.robot_status == "Stop":
                break


    def init_services(self):
        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.get_robot_state("rodentrider", "")
            if not resp.success == True:
                self.robot_status = "no_robot"


        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "no_robot"
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/remove_model", 5.0)
            self.remove_model = rospy.ServiceProxy('remove_model', RemoveModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


        try:
            rospy.wait_for_service("/spawn_model", 5.0)
            self.spawn_model = rospy.ServiceProxy('spawn_model', SpawnModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def add_wall(self):
        final_position = Vector3D(0,0,0)
        yaw = 0
        spawn_name = "wall"
        self.spawn_model("dropping_wall", spawn_name, final_position.x, final_position.y, final_position.z, yaw) # create the model

    def remove_wall(self):
        self.remove_model("wall")
  
    def handle_robot(self,req):

        if self.robot_status == "Paused":
            self.robot_status = "Running"
            self.start_time = rospy.get_time()

            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Running":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )


    def publish_metrics(self):
        self.metrics['Status'] = self.robot_status
        #if self.out_of_sequence:
        #    self.metrics['Status'] = "Gates Out of Sequence"

        # round will make it show as an int (39.0000) for example - need decimal precision
        display_time = (float)((int)(self.time_passed*100.0))/100.0
        self.metrics['Time'] = display_time # round(self.time_passed, 2)

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))


    def send_to_api(self, score=0.0, disqualified=False,  **kwargs):
        simulation_id = os.environ.get('RIDERS_SIMULATION_ID', None)
        args = score, disqualified
        try:
            if simulation_id:
                self.send_simulation_results(simulation_id, *args, **kwargs)
            else:
                self.send_branch_results(*args, **kwargs)
        except Exception as e:
            rospy.loginfo('Exception occurred while sending metric: %s', e)
        

    def send_simulation_results(self, simulation_id, score, disqualified, **kwargs):
        host = os.environ.get('RIDERS_HOST', None)
        token = os.environ.get('RIDERS_SIMULATION_AUTH_TOKEN', None)
        create_round_url = '%s/api/v1/simulation/%s/rounds/' % (host, simulation_id)
        kwargs['score'] = kwargs.get('score', score)
        kwargs['disqualified'] = kwargs.get('disqualified', disqualified)
        data = {
            'metrics': json.dumps(kwargs)
        }
        # send actual data
        requests.post(create_round_url, {}, data, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    def send_branch_results(self, score, disqualified, **kwargs):
        rsl_host = 'http://localhost:8059'
        # metrics endpoint stores data differently compared to simulation endpoint,
        # hence we change how we send it
        info = {
            'score': score,
            'disqualified': disqualified,
        }
        info.update(kwargs)
        url = '%s/events' % rsl_host
        data = {
            'event': 'metrics',
            'info': json.dumps(info)
        }

        requests.post(url, {}, data, headers={
            'Content-Type': 'application/json',
        })


    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

if __name__ == '__main__':
    # Name of robot
    controller = GameController("rodentrider")
